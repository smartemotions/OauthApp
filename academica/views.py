from django.http import JsonResponse
from oidc_provider.lib.utils.oauth2 import protected_resource_view

from academica.serializers import *
from rest_framework.viewsets import ModelViewSet
from academica.models import *
from django.views.decorators.http import require_http_methods
from oauth2_provider.views.generic import ProtectedResourceView
"""
# *******************************************************
# PASO 1 -------->
# Serializers academica App
Vistas modelos Academica App
Cada vista tiene metodos HTTP(S) limitados


# *******************************************************
# PASO 3 -------->
Adiciona protección Oauth2 a recursos REST
Heredar de ProtectedResourceView + ModelViewset
"""


class NoteViewSet(ProtectedResourceView, ModelViewSet):
    """
    Encapsula las vistas para el modelo nota
    incluye los métodos HTTP (GET, POST, PUT, PATCH, DELETE, HEAD),
    más las restricciones del serializador del modelo
    y los metodos permitidos configurados
    en la anotación
    """
    def get_queryset(self):
        return Note.objects.filter(
            student=self.request.user.id
        )
    serializer_class = NoteSerializer
    http_method_names = ['get']


class StudentViewSet(ProtectedResourceView, ModelViewSet):
    """
    Encapsula las vistas para el modelo estudiante (student)
    los métodos permitidos se limitan
    a GET y PUT
    """
    def get_queryset(self):
        return Student.objects.filter(
            id=self.request.user.id
        )
    serializer_class = StudentSerializer
    http_method_names = ['get', 'put']


@require_http_methods(['GET'])
@protected_resource_view(['read_notes'])
def get_student_notes(request, *args, **kwargs):
    """ Vista de notas usuario autenticado OIDC
    PASO 4 -------->
    :param request:
    :param args:
    :param kwargs: Argumentos arbitrarios que
    contienen Token usuario entre
    otros parametros
    :return: Lista de notas estudiante autenticado en
    formato JSON
    """
    token = kwargs['token']
    queryset = Note.objects.filter(student=token.user.id)
    queryset = NoteSerializer(data=queryset, many=True)
    queryset.is_valid()
    return JsonResponse(queryset.data, safe=False)
