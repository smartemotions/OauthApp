from academica.views import *
from rest_framework import routers
from django.conf.urls import url


"""
# *******************************************************
# PASO 2 -------->
# URLS academica App
Configuración de rutas REST
vistas Academica App
"""

router = routers.SimpleRouter()
router.register(r'note', NoteViewSet, base_name="notes")
router.register(r'student', StudentViewSet, base_name="students")

urlpatterns = [
    # PASO 4 -------->
    # Controlador vista notas estudiante para OIDC
    url(r'^student/note', get_student_notes, name="student_notes"),
]
urlpatterns += router.urls
