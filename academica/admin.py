from django.contrib import admin
from academica.models import *
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _


# Register your models here.
"""
# *******************************************************
# PASO 1 -------->
# Configuración agregar modelos a vista administrador DJANGO App
"""


@admin.register(User)
class Studentdmin(DjangoUserAdmin):
    list_display = ('username',
                    'first_name',
                    'last_name',
                    'is_staff',
                    'is_superuser')
    fieldsets = (
        (
            _('Información de la Cuenta'),
            {'fields': ('username', 'email', 'password')}
        ),
        (
            _('Personal info'),
            {'fields': ('first_name',
                        'last_name',
                        'student_profile',
                        'student_age',
                        'student_gender')
             }
        ),
        (
            _('Información Academica'),
            {'fields': ('student_code', 'student_semester')}
        ),
        (
            _('Permissions'),
            {'fields': ('is_active', 'is_staff',
                        'is_superuser', 'groups',
                        'user_permissions')
             }),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )


class SubjectAdmin(admin.ModelAdmin):
    list_display = ('subject_name',)
    fields = ('subject_name',)


class NoteAdmin(admin.ModelAdmin):
    list_display = ('student', 'subject', 'note_final')
    fields = ('student', 'subject', 'note_final')


admin.site.register(Student, Studentdmin)
admin.site.register(Subject, SubjectAdmin)
admin.site.register(Note, NoteAdmin)
