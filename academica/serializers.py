from rest_framework.serializers import ModelSerializer
from academica.models import *
"""
# *******************************************************
# PASO 1 -------->
# Serializers academica App
Serializers para convertir instancias de modelos a objetos nativos Python
y poder renderizarlos en Objetos JSON
"""


class SubjectSerializer(ModelSerializer):
    """
    Encapsular el Objeto JSON del modelo Asignatura
    Los campos serializados se especifican en fieds
    """
    class Meta:
        model = Subject
        fields = ('subject_name',)


class StudentSerializer(ModelSerializer):
    """
    Encapsula el Objeto JSON del modelo Estudiante (Student)
    Los campos de solo lectura se especifican
    en read_only_fields
    """
    class Meta:
        model = Student
        read_only_fields = ('first_name',
                            'last_name',
                            'student_code',
                            'student_age',
                            'student_semester',)
        fields = ('id',
                  'first_name',
                  'last_name',
                  'email',
                  'student_profile',
                  'student_code',
                  'student_age',
                  'student_gender',
                  'student_semester',)


class NoteSerializer(ModelSerializer):
    subject = SubjectSerializer(many=False, read_only=True)

    class Meta:
        model = Note
        read_only_fields = ('note_final',)
        fields = ('note_final', 'subject')
