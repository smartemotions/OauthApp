# Oauth DJango Ejemplo

Ejecutar para cambiar de rama 
````commandline
git checkout <branch-name>
````
## Configurando Proyecto
Realizar las migraciones para sincronizar con los datos
````commandline
pip install -r requirements.txt
python manage.py migrate
python manage.py createsuperuser
````

## DJango REST Framework
Migrando modelos creados en la app academica
````commandline
python manage.py migrate academica
````
## Heroku Admin

https://oidcon.herokuapp.com/admin

Usuario: user
Password:    Hola1234

# Nota

Esta versión es de prueba
y no cuenta con ningun parametro de
seguridad, los servicios se
deben utilizar entendiendo que
no hay restricciones activas