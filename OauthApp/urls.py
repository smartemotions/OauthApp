from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include
from django.conf.urls.static import static
from .Settings.base import MEDIA_ROOT, MEDIA_URL
from django.views.generic import TemplateView
"""
OauthApp URL Configuration
"""

urlpatterns = [
    path('admin/', admin.site.urls),

    # PASO 1 -------->
    url(r'^$', TemplateView.as_view(template_name='home.html'), name='home'),
    url(r'^academica/', include("academica.urls")),


    # PASO 3 -------->
    url(r'^oauth/', include('oauth2_provider.urls',
                            namespace='oauth')),

    # PASO 4 -------->
    url(r'^', include('oidc_provider.urls',
                      namespace='oidc_provider')),
    url(r'^accounts/', include('registration.backends.default.urls')),
]

urlpatterns += static(MEDIA_URL, document_root=MEDIA_ROOT)
