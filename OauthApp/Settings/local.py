from .base import *
import dj_database_url
ENVIRONMENT = 'local'
DEBUG = True
DATABASES = {'default': dj_database_url.config()}

