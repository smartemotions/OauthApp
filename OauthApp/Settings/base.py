import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'bk_oe@oiu6(o!5p$a+ji#!$6qrxdrrvt1wqpcfgw##q+7z+*zm'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = ['https://oidcon.herokuapp.com', 'http://oidcon.herokuapp.com']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # *******************************************************
    # PASO 1 -------->
    # Iniciando Proyecto
    'academica',


    # *******************************************************
    # PASO 2 -------->
    # Rest Framework
    'rest_framework',
    'corsheaders',


    # *******************************************************
    # PASO 3 -------->
    # Oauth App Configuración
    'oauth2_provider',


    # *******************************************************
    # PASO 4 -------->
    # OIDC App Configuración
    'oidc_provider',
    'registration',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    # *******************************************************
    # Configuración Personalizada...
    # PASO 2 -------->
    # Rest Framework software intermedio permitir dominios cruzados
    'corsheaders.middleware.CorsMiddleware',


    # *******************************************************
    # PASO 3 -------->
    # Oauth App Configuración
    # Software Intermedio infraestructura Oauth peticiones cliente
    'oauth2_provider.middleware.OAuth2TokenMiddleware',


    # *******************************************************
    # PASO 4 -------->
    # OIDC App Configuración
    # Software Intermedio gestión de sesión cuentas de suaurios
    'oidc_provider.middleware.SessionManagementMiddleware',

    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'OauthApp.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'OauthApp.wsgi.application'


# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default':
        {
            'ENGINE': 'django.db.backends.postgresql',
            'NAME': 'AuthApp',
            'USER': 'postgres',
            'PASSWORD': 'postgres',
            'HOST': '127.0.0.1',
            'PORT': '5432',
        }
}


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'es-CO'

TIME_ZONE = 'America/Bogota'

USE_I18N = True

USE_L10N = True

USE_TZ = True


"""
# *******************************************************
# Configuración Personalizada...
# PASO 1 -------->
"""
# URL archivos estaticos JS, CSS...
STATIC_URL = '/static/'
# URL archivos media imagenes, video...
MEDIA_URL = '/media/'
# STATIC_ROOT = os.path.join(BASE_DIR, 'static')
# Directorio archivos estaticos
STATICFILES_DIRS = [
    os.path.join(BASE_DIR, "static"),
]
# Directorio archivos media (almacena fotos cuentas usuarios)
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# IMPORTANTE...
# Configuración para autenticarse con Modelo Personalizado
AUTH_USER_MODEL = 'academica.Student'


"""
# *******************************************************
# PASO 2 -------->
# Rest Framework Configuración
"""
REST_FRAMEWORK = {
    # Permite extraer información usuario autenticado
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        # Permitir acceso solo usuarios autenticados
        # 'rest_framework.permissions.IsAuthenticated',
    ),
}

# Permisos de Dominios Cruzados
CORS_ORIGIN_ALLOW_ALL = True


"""
# *******************************************************
# PASO 3 -------->
# Permite extraer los valores del usuario autenticado
# por medio de Oauth
"""
AUTHENTICATION_BACKENDS = [
    'oauth2_provider.backends.OAuth2Backend',
    'django.contrib.auth.backends.ModelBackend',
]


"""
# *******************************************************
# PASO 4 -------->
# Configuración tiempo expirar Código de autorización 10 Segundos
# Configuración tiempo expirar Toke expresado en segundos 5 Minutos
# Configuración reclamaciones servicio user-info para OIDC
"""
OIDC_CODE_EXPIRE = 60
OIDC_TOKEN_EXPIRE = 60
OIDC_USERINFO = 'OauthApp.Settings.oidc_provider_settings.userinfo'

# IMPORTANTE
# URI vista de autenticación
LOGIN_URL = '/accounts/login'
LOGIN_REDIRECT_URL = '/'
