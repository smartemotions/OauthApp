import os
from celery import Celery
os.environ.setdefault('DJANGO_SETTINGS_MODULE',
                      'OauthApp.Settings.base')
app = Celery('OauthApp')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks()
